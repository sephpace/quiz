# Source directories
SRCDIR   = src/
# Object directory
OBJDIR	 = obj/
# Include directory
INCDIR   = include/
# Output binary directory
BINDIR   = bin/
# Output file name
TARGET   = quiz
# Compiler
CXX      = g++
# Compiler flags
CXXFLAGS = `pkg-config --cflags --libs ncurses`

# Source files
SRC      = $(wildcard $(SRCDIR)*.cpp)
# Object files
OBJ      = $(patsubst $(SRCDIR)%,$(OBJDIR)%,$(patsubst %.cpp,%.o,$(SRC)))

# Phony targets
.PHONY: clean debug

# Link all object files
$(TARGET) : $(OBJ)
	@echo Linking $(OBJ)...
	@mkdir -p $(BINDIR)
	@$(CXX) $(CXXFLAGS) $(OBJ) -o $(BINDIR)$@
	@echo Output saved to $(BINDIR)$(TARGET)

# Compile all source files
$(OBJDIR)%.o : $(SRCDIR)%.cpp
	@echo Compiling $< into $@...
	@mkdir -p $(OBJDIR)
	@$(CXX) $(CXXFLAGS) $(DEBUG) -c $< -o $@ $(addprefix -I,$(INCDIR))

# Clean up output files
clean :
	@$(RM) -r $(OBJDIR)
	@$(RM) -r $(BINDIR)

# Add debug flag then recompile
debug : DEBUG = -g
debug : clean $(TARGET)
