/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef CARD_HPP
#define CARD_HPP


#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "utils.hpp"


/* A card contains information for a specific flash card. The definition is a
 * description of a term or a "question" to be answered. The answers are a list
 * of possible answers to the definition. There may be a minimum of 1 answer
 * with no max limit. Hints are optional.
 */
class Card
{
  /* The answer(s) to the definition on the card. */
  std::vector<std::string> answers;

  /* The definition on the card. */
  std::string definition;

  /* The hint for the card. */
  std::string hint;

  /* The card id. */
  unsigned long id;

  public:

    /* Default constructor. Creates an empty card. */
    Card();

    /* Constructor without hint.
     *
     * Parameters:
     *    answers    - The answer(s) for the card.
     *    definition - The definition for the card.
     */
    Card(unsigned long id, std::vector<std::string> &answers, std::string definition);

    /* Constructor with hint.
     *
     * Parameters:
     *    answers    - The answer(s) for the card.
     *    definition - The definition for the card.
     *    hint       - The hint for the card.
     */
    Card(unsigned long id, std::vector<std::string> &answers, std::string definition, std::string hint);
    
    /* Greater than operator overload. Compares card ids.
     *
     * Parameters:
     *    card - The card on the right side of the equation.
     * 
     * Returns:
     *    The result of the operation.
     */
    const bool operator>(Card &card) const;

    /* Less than operator overload. Compares card ids.
     *
     * Parameters:
     *    card - The card on the right side of the equation.
     * 
     * Returns:
     *    The result of the operation.
     */
    const bool operator<(Card &card) const;
    
    /* Checks if the given answer is correct. If any answer in the list of answers
     * matches it is considered correct.
     *
     * Parameters:
     *    answer - The answer to check.
     *
     * Returns:
     *    true if the answer is correct and false otherwise.
     */
    const bool check_answer(std::string answer) const;
   
    /* Returns the answers for the card. */
    const std::vector<std::string> get_answers() const;

    /* Returns the card's definition. */
    const std::string get_definition() const;

    /* Returns the hint of the card. If no hint is found, "No hint" will be
     * returned instead.
     */
    const std::string get_hint() const;

    /* Returns the card's id. */
    const unsigned long get_id() const;

    /*
     * Stream extraction operator overload.
     *
     * Extracts the id, answers, definition, and hint from the given input stream and
     * inserts them into the given card.
     *
     * Parameters:
     *   stream - The input stream.
     *   card   - The card to extract.
     * 
     * Returns:
     *   The given input stream.
     */
    friend std::istream &operator>>(std::istream &stream, Card &card);

    /*
     * Stream insertion operator overload.
     *
     * Inserts a string representation of the given card in the form
     * "id:answer1,answer2,...,answerN:definition:hint\n" into
     * the given output stream.
     *
     * Parameters:
     *   stream - The output stream.
     *   card   - The card to insert.
     *
     * Returns:
     *   The given output stream.
     */
    friend std::ostream &operator<<(std::ostream &stream, const Card &card);
};


#endif // CARD_HPP
