/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef VIEW_HPP
#define VIEW_HPP


#include <vector>

#include <ncurses.h>

#include "window.hpp"


/* A view contains multiple windows and refreshes and resizes them so that they
 * look pleasant in the terminal.
 */
class View
{
  /* The windows displayed in the view. */
  std::vector<Window*> windows;

  public:

    /* Constructor. */
    View();

    /* Destructor. */
    virtual ~View();

    /* Adds a window to the view. The window will be displayed and refreshed
     * when changed.
     *
     * Parameters:
     *    window - The window to add to the view.
     */
    void add_window(Window *window);

    /* Clears each windows in the view. */
    void clear() const;

    /* Pauses the view until a key is pressed. */
    virtual void pause() const = 0;

    /* Refreshes each window in the view. */
    void refresh() const;

    /* Updates and refreshes each window in the view. */
    void update();


  protected:

    /* The current height of the terminal. */
    int term_height = 0;

    /* The current width of the terminal. */
    int term_width = 0;


  private:

    /* Checks if the windows need to be resized.
     *
     * Returns:
     *    true if the windows need to be resized and false otherwise.
     */
    bool check_resize();

    /* Resizes the windows so they fit to the current terminal size. */
    virtual void resize() = 0;
};


/* The view used for quizzing each card in a deck. */
class QuizView : public View
{
  /* The information bar at the top of the terminal */
  Window *info_bar;

  /* The prompt window where instructions/hints are displayed */
  CenteredWindow *prompt_window;

  /* The description window */
  CenteredWindow *descr_window;

  /* The input bar where the user types */
  InputField *input_bar;


  public:

    /* The amount of cards correctly guessed. */
    unsigned int correct = 0;

    /* The amount of cards incorrectly guessed. */
    unsigned int incorrect = 0;

    /* The amount of cards that still need to be drilled. */
    unsigned int remaining = 0;

    /* The amount of cards skipped. */
    unsigned int skipped = 0;

    /* Constructor. */
    QuizView();

    /* Destructor. */
    ~QuizView();

    /* Displays the given text in the description window.
     *
     * Parameters:
     *    text - The text to display.
     */
    void display_description(const std::string text) const;
    
    /* Displays the information in the info bar. */
    void display_info() const;

    /* Displays the given text in the prompt window.
     *
     * Parameters:
     *    text - The text to display.
     */
    void display_prompt(const std::string text) const;

    /* Returns an answer or command that was entered by the user.
     *
     * Returns:
     *    The answer/command from the user.
     */
    std::string get_answer() const;

    /* Pauses the view until a key is pressed. */
    void pause() const;


  private:

    /* Resizes the windows so they fit to the current terminal size. */
    void resize();
};


#endif // VIEW_HPP
