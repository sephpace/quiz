/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "card.hpp"


/* Default constructor. Creates an empty card. */
Card::Card()
{
  id = 0;
}

/* Constructor without hint. */
Card::Card(unsigned long id, std::vector<std::string> &answers, std::string definition)
{
  this->id = id;
  this->answers = answers;
  this->definition = definition;
  // TODO: Consider setting the hint to an empty string to save space in saved decks
  this->hint = "No hint";
}

/* Constructor with hint. */
Card::Card(unsigned long id, std::vector<std::string> &answers, std::string definition, std::string hint) : Card(id, answers, definition)
{
  this->hint = hint;
}

/* Greater than operator overload */
const bool Card::operator>(Card &card) const
{
  return id > card.get_id();
}

/* Less than operator overload */
const bool Card::operator<(Card &card) const
{
  return id < card.get_id();
}

/* Checks if the given answer is correct. */
const bool Card::check_answer(std::string answer) const
{
  // Convert the answer to lowercase
  answer = tolower(answer);
  for(std::string ans : answers)
  {
    if(tolower(ans) == answer)
    {
      return true;
    }
  }
  return false;
}

/* Returns the answers for the card. */
const std::vector<std::string> Card::get_answers() const
{
  return answers;
}

/* Returns the card's definition. */
const std::string Card::get_definition() const
{
  return definition;
}

/* Returns the hint or "No hint" if there is none. */
const std::string Card::get_hint() const
{
  return hint;
}

/* Returns the card's id. */
const unsigned long Card::get_id() const
{
  return id;
}

/* Stream extraction operator overload. */
std::istream &operator>>(std::istream &stream, Card &card)
{
  // Extract id
  std::stringbuf id_buf;
  stream.get(id_buf, ':');
  stream.get();
  unsigned long id = std::stol(id_buf.str());

  // Extract answers
  std::stringbuf answer_buf;
  stream.get(answer_buf, ':');
  stream.get();
  std::stringstream answer_stream(answer_buf.str());
  std::vector<std::string> answers;
  while(answer_stream)
  {
    std::stringbuf answer;
    answer_stream.get(answer, ',');
    answer_stream.get();
    answers.push_back(answer.str());
  }

  // Extract definition
  std::stringbuf definition_buf;
  stream.get(definition_buf, ':');
  stream.get();
  std::string definition = definition_buf.str();

  // Extract hint
  // TODO: Find out what happens when there is no hint
  std::stringbuf hint_buf;
  stream.get(hint_buf, ':');
  stream.get();
  std::string hint = hint_buf.str();

  // Set the card values
  card = Card(id, answers, definition, hint);

  return stream;
}

/* Stream insertion operator overload */
std::ostream &operator<<(std::ostream &stream, const Card &card)
{
  // Insert id
  stream << card.get_id() << ":";

  // Insert answers
  std::vector<std::string> answers = card.get_answers();
  for(int i = 0; i < answers.size(); i++)
  {
    stream << answers[i];
    if(i < answers.size() - 1)
    {
      stream << ",";
    }
  }
  stream << ":";

  // Insert definition
  stream << card.get_definition() << ":";

  // Insert hint
  stream << card.get_hint() << std::endl;

  return stream;
}
