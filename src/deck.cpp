/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "deck.hpp"

// The directory decks are saved to and loaded from
// TODO: Move this into a configuration file and replace /home/seph with $HOME
const std::string DECK_DIR = "/home/seph/.quiz/decks";


/* Constructor */
Deck::Deck(std::string name)
{
  this->cards = new std::vector<Card>();
  this->name = name;
}

/* Destructor */
Deck::~Deck()
{
  delete cards;
  cards = NULL;
}

/* Adds a card to the deck */
void Deck::add(const Card &card)
{
  cards->push_back(card);
}

/* Adds all of the given cards to the deck one by one. */
void Deck::add_all(const std::vector<Card> cards)
{
  for(Card card : cards)
  {
    add(card);
  }
}

/* Removes all cards from the deck. Resets the deck to its initial state. */
void Deck::clear()
{
  cards->clear();
}

/* Draws a card from the deck (removing it in the process). */
const Card Deck::draw()
{
  Card card = cards->back();
  cards->pop_back();
  return card;
}

/* Returns the name of the deck. */
const std::string Deck::get_name() const
{
  return name;
}

/* Loads the deck file with the given name. */
bool Deck::load(std::string name)
{
  // Load status
  bool loaded = false;

  // Open the deck file
  std::ifstream deck_file;
  std::string load_path = DECK_DIR + "/" + name + ".deck";
  deck_file.open(load_path);

  // Check if the deck exists 
  if(deck_file.is_open())
  {
    // Load all cards from the deck file
    std::vector<Card> cards;
    std::string line;
    while(std::getline(deck_file, line))
    {
      std::stringstream card_str(line);
      Card card;
      card_str >> card;
      cards.push_back(card);
    }

    // Overwrite the current deck
    clear();
    add_all(cards);
    loaded = true;
  }
  
  // Close the file
  deck_file.close();

  // Return status
  return loaded; 
}

/* Saves the deck to a deck file. */
void Deck::save()
{
  // Sort the deck
  sort();
 
  // Open the deck file
  std::ofstream deck_file;
  std::string load_path = DECK_DIR + "/" + name + ".deck";
  deck_file.open(load_path);
 
  // Check if the deck was opened successfully
  if(deck_file.is_open())
  {
    // Save each card to the deck file
    for(Card card : *cards)
    {
      deck_file << card; 
    }
  }
  else
  {
    std::cout << "Error opening deck file!" << std::endl;
    exit(1);
  }

  // Close the file
  deck_file.close();
}

/* Shuffles the deck. */
void Deck::shuffle()
{
  std::random_device rd;
  std::default_random_engine rng(rd());
  std::shuffle(cards->begin(), cards->end(), rng);
}

/* Returns the amount of cards in the deck. */
const int Deck::size() const
{
  return cards->size();
}

/* Sorts the deck by card ids */
void Deck::sort()
{
  std::sort(cards->begin(), cards->end());
}

