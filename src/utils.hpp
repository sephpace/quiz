/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef UTILS_HPP
#define UTILS_HPP


#include <algorithm>
#include <string>
#include <vector>


/* Splits the given string by spaces.
 *
 * Parameters:
 *    str - The string to split.
 *
 * Returns:
 *    A vector containing the split substrings.
 */
const std::vector<std::string> split(const std::string str);

/* Splits the given string by the given delimiter.
 *
 * Parameters:
 *    str - The string to split.
 *    del - The delimeter string to split by.
 * 
 * Returns:
 *    A vector containing the split substrings.
 */
const std::vector<std::string> split(const std::string str, const std::string del);

/* Converts the given string to lowercase.
 *
 * Parameters:
 *    str - The string to convert.
 *
 * Returns:
 *    The lowercase version of the string.
 */
const std::string tolower(std::string str);


#endif // UTILS_HPP
