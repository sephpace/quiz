/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "quiz.hpp"

int main()
{
  // Start the quizzer
  Quizzer quizzer("test_deck0");
  quizzer.start();

  // Return upon success
  return 0;
}
