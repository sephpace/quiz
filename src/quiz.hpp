/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef QUIZ_HPP
#define QUIZ_HPP


#include <string>
#include <vector>

#include "card.hpp"
#include "deck.hpp"
#include "gui.hpp"
#include "view.hpp"
#include "window.hpp"


/* A quizzer is used to drill each flashcard in a deck until the user has
 * answered correctly for each one. An ncurses gui is used to display cards
 * and get input from the user.
 */
class Quizzer : public GUI
{
  /* The current card being displayed. */
  Card card;

  /* The deck of flashcards to drill. */
  Deck *deck;

  /* The discard pile, i.e. cards that were answered wrong to be drilled again. */
  std::vector<Card> discard;

  /* The main view for the quizzer. */
  QuizView *quiz_view;


  public:
 
    /* Constructor with deck name. Loads a deck for quizzing.
     *
     * Parameters:
     *    deck_name - The name of the deck to quiz the user on.
     */
    Quizzer(std::string deck_name);

    /* Destructor */
    ~Quizzer();


  private:

    /* Determines if the given string is a command and if so, runs the command.
     *
     * Parameters:
     *    command - The string to parse.
     *
     * Returns:
     *    true if the string was a command and false otherwise.
     */
    bool parse_command(const std::string command);

    /* Quizzes the user on a single pass of each card in the deck. */
    void quiz();

    /* Updates the quizzer GUI. */
    void update();
};

#endif // QUIZ_HPP
