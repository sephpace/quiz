/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef WINDOW_HPP
#define WINDOW_HPP


#include <stdarg.h>
#include <string>
#include <vector>

#include <ncurses.h>

#include "utils.hpp"


/* A window is a wrapper object for an ncurses WINDOW. It keeps track of the
 * text present in the window and updates it when necessary.
 */
class Window
{
  public:
    /* Determines if borders should be drawn for the window. */
    bool has_borders = false;

    /* The margin on the outside of the border of the window. */
    int margin = 0;

    /* The padding on the inside of the border of the window. */
    int padding = 0;

    /* Constructor.
     *
     * Parameters:
     *    height - The number of lines in the window.
     *    width  - The number of columns in the window.
     *    y      - The vertical position of the top-left corner of the window.
     *    x      - The horizontal position of the top-left corner of the window.
     */
    Window(int height, int width, int y, int x);

    /* Destructor. */
    virtual ~Window();

    /* Clears the window of all text. */
    void clear();

    /* Assigns the given integers to the values of the height and width of the
     * window.
     *
     * Parameters:
     *    height - The value to store the height of the window in.
     *    width  - The value to store the width of the window in.
     */
    void get_size(unsigned int &height, unsigned int &width);

    /* Returns the text in the window. */
    std::string get_text() const;

    /* Moves the window to the specified location.
     *
     * Parameters:
     *    y - The line number to set the position to.
     *    x - The column number to set teh position to.
     */
    void move(int y, int x);

    /* Pauses the GUI on the window until a key is pressed. */
    void pause() const;

    /* Displays the given text in the window. */
    virtual void print(std::string text);

    /* Formats the text with the given parameters (similar to the standard C
     * printf function) and displays it in the window. 
     *
     * Parameters:
     *    template - The formatting template string. 
     *    ...      - Any parameters to format.
     */
    void printf(std::string ftext, ...);

    /* Refreshes the window. */
    void refresh();

    /* Sets the window's size to the given values.
     *
     * Parameters:
     *    height - The window height in lines.
     *    width  - The window width in columns.
     */
    void resize(int height, int width);

    /* Resizes and moves the window.
     *
     * Parameters: 
     *    height - The window height in lines.
     *    width  - The window width in columns.
     *    y - The line number to set the position to.
     *    x - The column number to set teh position to.
     */
    void resize_and_move(int height, int width, int y, int x);

    /* Sets the cursor position to the given location.
     *
     * Parameters:
     *    y - The line number to set the cursor to.
     *    x - The column number to set the cursor to.
     */
    void set_cursor(int y, int x);

    /* Enable or disable scrolling.
     *
     * Parameters:
     *    value - The value to set scrolling to (true/false).
     */
    void set_scroll(bool value);


  protected:

    /* The text in the window. Redisplayed on refreshes. */
    std::string text;

    /* The ncurses window object. */
    WINDOW *window;

    
  private:

    /* Displays the borders of the window (if it has any). */
    void display_borders();
};


/* A window where the text is always displayed in the center. */
class CenteredWindow : virtual public Window
{
  public:
   
    /* Constructor.
     *
     * Parameters:
     *    height - The number of lines in the window.
     *    width  - The number of columns in the window.
     *    y      - The vertical position of the top-left corner of the window.
     *    x      - The horizontal position of the top-left corner of the window.
     */
    CenteredWindow(int height, int width, int y, int x);

    /* Destructor. */
    virtual ~CenteredWindow();

    /* Displays the given text in the center of the window.
     *
     * Parameters:
     *    text - The text to display.
     */
    virtual void print(std::string text);
};


/* A special window that is used to get user input. */
class InputField : public Window
{
  public:

    /* Constructor.
     *
     * Parameters:
     *    height - The number of lines in the window.
     *    width  - The number of columns in the window.
     *    y      - The vertical position of the top-left corner of the window.
     *    x      - The horizontal position of the top-left corner of the window.
     */
    InputField(int height, int width, int y, int x);

    /* Destructor. */
    ~InputField();
 
    /* Polls input from the user.
     *
     * Returns:
     *    The user input string.
     */
    const std::string get_input();
};


#endif // WINDOW_HPP
