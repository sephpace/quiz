/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "window.hpp"


/* Constructor. */
Window::Window(int height, int width, int y, int x)
{
  window = newwin(height, width, y, x);
}

/* Destructor. */
Window::~Window()
{
  delwin(window);
  window = NULL;
}

/* Clears the window of all text. */
void Window::clear()
{
  text.clear();
  wclear(window);
}

/* Assigns the given integers to the values of the height and width of the
 * window. */
void Window::get_size(unsigned int &height, unsigned int &width)
{
  getmaxyx(window, height, width);
}

/* Returns the text in the window. */
std::string Window::get_text() const
{
  return text;
}


/* Moves the window to the specified location. */
void Window::move(int y, int x)
{
  mvwin(window, y, x);
}

/* Pauses the GUI on the window until a key is pressed. */
void Window::pause() const
{
  noecho();
  wgetch(window);
  echo();
}

/* Displays the given text in the window. */
void Window::print(std::string text)
{
  this->text += text;
  waddstr(window, text.c_str());
}

/* Formats the text with the given parameters. */
void Window::printf(std::string ftext, ...)
{
  // Get current window size
  unsigned int height, width;
  get_size(height, width);

  // Create the text from ftext and params
  int buf_size = height * width;
  char buffer[buf_size];
  va_list params;
  va_start(params, ftext);
  vsprintf(buffer, ftext.c_str(), params);
  va_end(params);
  std::string text(buffer);

  // Print the text to the window
  print(text);
}

/* Refreshes the window. */
void Window::refresh()
{
  // Save the current text and clear
  std::string text_buffer = text;
  clear();

  // Redraw the borders if necessary
  if(has_borders)
  {
    display_borders();
  }

  // Redraw the text
  print(text_buffer);

  // Refresh the window
  wrefresh(window);
}

/* Sets the window's size to the given values. */
void Window::resize(int height, int width)
{
  wresize(window, height, width);
}

/* Resizes and moves the window. */
void Window::resize_and_move(int height, int width, int y, int x)
{
  this->resize(height, width);
  this->move(y, x);
}

/* Sets the cursor position to the given location. */
void Window::set_cursor(int y, int x)
{
  wmove(window, y, x);
}

/* Enable or disable scrolling. */
void Window::set_scroll(bool value)
{
  scrollok(window, value);
}

/* Displays the borders of the window (if it has any). */
void Window::display_borders()
{
  box(window, 0, 0);
}


/* Constructor. */
CenteredWindow::CenteredWindow(int height, int width, int y, int x)
  : Window(height, width, y, x) {}

/* Destructor. */
CenteredWindow::~CenteredWindow() {}

/* Displays the given text in the center of the window. */
void CenteredWindow::print(std::string text)
{
  // Find window height and width
  unsigned int height, width;
  get_size(height, width);

  // TODO: Make sure it handles lines that are longer than the window width
  // correctly (i.e. if a single word is longer than the window itself).

  // Split the text into optimally-sized lines std::vector<std::string> lines;
  std::vector<std::string> lines;
  for(std::string line : split(text, "\n"))
  {
    // Check if the line needs to be split
    if(line.size() <= width)
    {
      // No need to split
      lines.push_back(line);
    }
    else
    {
      // Split the line until each segment fits in the window
      std::string new_line;
      for(std::string word : split(line))
      {
        // Start the new line again once larger than the window width
        if(new_line.size() + word.size() + padding * 2 + 1 > width)
        {
          // Add the new line to the final lines
          lines.push_back(new_line);
          new_line.clear();
        }

        // Add the word to the new line 
        new_line += word + " ";
      }

      // Add any remaining words
      lines.push_back(new_line);
    }
  }

  // Display each line, centered vertically and horizontally
  int start_y = (height / 2) - (lines.size() / 2);
  int start_x;
  for(int i = 0; i < lines.size(); i++)
  {
    // Display the line
    start_x = (width / 2) - (lines[i].size() / 2);
    set_cursor(start_y + i, start_x);
    Window::print(lines[i]);

    // Add newline to the window text for repeatability
    this->text += '\n';
  }
}


/* Constructor. */
InputField::InputField(int height, int width, int y, int x)
  : Window(height, width, y, x) {}

/* Destructor. */
InputField::~InputField() {}

/* Polls input from the user. */
const std::string InputField::get_input()
{
  // Clear the input field 
  clear();

  // Create input buffer
  unsigned int h, w;
  get_size(h, w);
  unsigned int buf_size = h * w;
  char input[buf_size];

  // Poll the user for input
  if(wgetnstr(window, input, buf_size))
  {
    return "";
  }
  else
  {
    return std::string(input);
  }
}
