/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef DECK_HPP
#define DECK_HPP


#include <fstream>
#include <random>
#include <string>
#include <vector>

#include "card.hpp"


/* A deck is used to contain and manipulate sets of cards. Cards are ordered
 * but can be shuffled or sorted. Cards can be added or drawn from the deck
 * (which removes it from the deck).
 */
class Deck
{
  /* The cards in the deck */
  std::vector<Card> *cards;

  /* The name of the deck */
  std::string name;


  public:

    /* Constructor. Creates an empty deck.
     *
     * Parameters:
     *    name - The name of the deck.
     */
    Deck(std::string name);

    /* Destructor */
    ~Deck();

    /* Adds a card to the deck.
     *
     * Parameters:
     *    card - The card to add to the deck.
     */
    void add(const Card &card);

    /* Adds all of the given cards to the deck one by one.
     *
     * Parameters:
     *    cards - The cards to add to the deck.
     */
    void add_all(const std::vector<Card> cards);

    /* Removes all cards from the deck. Resets the deck to its initial state. */
    void clear();

    /* Draws a card from the top of the deck (removing it in the process).
     *
     * Returns:
     *    The card from the top of the deck.
     */
    const Card draw();

    /* Returns the name of the deck. */
    const std::string get_name() const;

    /* Loads the deck file with the given name. Deck files are typically located 
     * in the directory "$HOME/.quiz/decks/" and are named "<deck_name>.deck".
     *
     * Replaces all cards currently in the deck with the ones saved in the deck
     * file with the specified name.
     *
     * Parameters:
     *    name - The name of the deck to load.
     * 
     * Returns:
     *    true if the file exists and was loaded and false if it does not exist.
     */
    bool load(std::string name);

    /* Saves the deck to a deck file. Deck files are typically saved in the
     * directory "$HOME/.quiz/decks/" and are named "<deck_name>.deck".
     *
     * Uses the current name of the deck to create the file but appends ".deck" to
     * the file name.
     *
     * Decks are sorted before being saved.
     */
    void save();

    /* Shuffles the deck. */
    void shuffle();

    /* Returns the amount of cards in the deck. */
    const int size() const;

    /* Sorts the deck by card ids */
    void sort();
};


#endif // DECK_HPP
