/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "view.hpp"


/* Constructor. */
View::View() {}

/* Destructor. */
View::~View() {}

/* Adds a window to the view. */
void View::add_window(Window *window)
{
  windows.push_back(window);
}

/* Clears each windows in the view. */
void View::clear() const
{
  for(Window *window : windows)
  {
    window->clear();
  }
}

/* Refreshes each window in the view. */
void View::refresh() const
{
  for(Window *window : windows)
  {
    window->refresh();
  }
}

/* Updates and refreshes each window in the view. */
void View::update()
{
  // Check for resizes
  if(check_resize())
  {
    this->resize();
  }

  // Refresh the view
  this->refresh();
}

/* Checks if the windows need to be resized. */
bool View::check_resize()
{
  // Get new terminal height and width
  int height, width;
  getmaxyx(stdscr, height, width);

  // Determine if windows need to be resized
  if(height != term_height || width != term_width)
  {
    // Update parent sizes
    term_height = height;
    term_width = width;

    return true;
  }

  return false;
}


/* Constructor. */
QuizView::QuizView()
{ 
  // Set up windows
  info_bar = new Window(0, 0, 0, 0);
  prompt_window = new CenteredWindow(0, 0, 0, 0);
  descr_window = new CenteredWindow(0, 0, 0, 0);
  input_bar = new InputField(0, 0, 0, 0);

  // Add windows to view
  add_window(info_bar);
  add_window(prompt_window);
  add_window(descr_window);
  add_window(input_bar);

  // Set margins and padding
  info_bar->margin = 2;
  prompt_window->margin = 1;
  descr_window->margin = 1;
  input_bar->margin = 1;
  prompt_window->padding = 2;
  descr_window->padding = 2;

  // Add borders
  prompt_window->has_borders = true;
  descr_window->has_borders = true;

  // Disable scrolling
  info_bar->set_scroll(false);
  prompt_window->set_scroll(false);
  descr_window->set_scroll(false);
  input_bar->set_scroll(false);
}

/* Destructor. */
QuizView::~QuizView()
{
  delete info_bar;
  delete prompt_window;
  delete descr_window;
  delete input_bar;
  info_bar = NULL;
  prompt_window = NULL;
  descr_window = NULL;
  input_bar = NULL;
}

/* Displays the given text in the description window. */
void QuizView::display_description(const std::string text) const
{
  descr_window->clear();
  descr_window->print(text);
}

/* Displays the information in the top bar. */
void QuizView::display_info() const
{
    info_bar->clear();
    std::string info = "Remaining: %u  Correct: %u  Incorrect: %u  Skipped: %u";
    info_bar->printf(info, remaining, correct, incorrect, skipped);
}

/* Displays the given text in the prompt window. */
void QuizView::display_prompt(const std::string text) const
{
  prompt_window->clear();
  prompt_window->print(text);
}

/* Returns an answer or command that was entered by the user.
 *
 * Returns:
 *    The answer/command from the user.
 */
std::string QuizView::get_answer() const
{
  return input_bar->get_input();
}

/* Pauses the view until a key is pressed. */
void QuizView::pause() const
{
  input_bar->pause();
}

/* Resizes the windows so they fit to the current terminal size. */
void QuizView::resize()
{
  // TODO: Make resizing dynamic to the sizes of the window

  // Set new window sizes
  // TODO: Move prompt_width somewhere better (like in a window class)
  int prompt_width = 20;
  info_bar->resize(1, term_width - info_bar->margin * 2);
  prompt_window->resize(term_height - prompt_window->margin * 2, prompt_width);
  descr_window->resize
  (
      term_height - descr_window->margin * 2,
      term_width - descr_window->margin * 3 - prompt_width
  );
  input_bar->resize(1, term_width - input_bar->margin * 2);

  // Move windows to new locations
  // TODO: Optimize this so windows aren't being moved unnecessarily
  info_bar->move(0, info_bar->margin);
  prompt_window->move(1, prompt_window->margin);
  descr_window->move(1, prompt_width + descr_window->margin * 2);
  input_bar->move(term_height - 1, input_bar->margin);
}
