/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "gui.hpp"


/* Constructor. */
GUI::GUI()
{
  // Initialize ncurses
  initscr();
}

/* Destructor. */
GUI::~GUI() {}

/* Starts the GUI application. */
void GUI::start()
{
  // Run the main loop
  while(!stopped)
  {
    update();
  }

  // Close out ncurses
  endwin();
}

/* Stops the GUI application. */
void GUI::stop()
{
  stopped = true;
}

/* Clear all windows in the main view. */
void GUI::clear()
{
  view->clear();
}

/* Checks if the GUI has been stopped. */
bool GUI::has_stopped()
{
  return stopped;
}

/* Pauses the gui until a key is pressed. */
void GUI::pause() const
{
  view->pause();
}

/* Refreshes all views. */
void GUI::refresh() const
{
  view->refresh();
}

/* Designates the given view to be the currently displayed view. */
void GUI::set_view(View *view)
{
  this->view = view;
}
