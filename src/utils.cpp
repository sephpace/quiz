/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "utils.hpp"


/* Splits the given string by spaces. */
const std::vector<std::string> split(const std::string str)
{
  return split(str, " ");
}

/* Splits the given string by the given delimiter. */
const std::vector<std::string> split(const std::string str, const std::string del)
{
  std::vector<std::string> substrings;
  std::string temp_str = str;
  size_t pos;
  while((pos = temp_str.find(del)) != std::string::npos)
  {
    substrings.push_back(temp_str.substr(0, pos));
    temp_str.erase(0, pos + del.size());
  }
  substrings.push_back(temp_str);
  return substrings;
}

/* Converts the given string to lowercase. */
const std::string tolower(std::string str)
{
  std::transform(str.begin(), str.end(), str.begin(), [](char c){ return std::tolower(c); });
  return str;
}
