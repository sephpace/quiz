/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#ifndef GUI_HPP
#define GUI_HPP


#include <vector>

#include <ncurses.h>

#include "view.hpp"


/* A GUI is an abstract class used to build ncurses applications. It contains
 * one or more views that can be switched between. Any methods that are needed
 * accross all views will be contained in this class.
 */
class GUI
{
  /* Signifies if the quizzer has been stopped. */
  bool stopped = false;

  /* The view that is currently selected and being displayed. */
  View *view;


  public:

    /* Constructor. */
    GUI();

    /* Destructor. */
    virtual ~GUI();

    /* Starts the GUI application. */
    void start();

    /* Stops the GUI application. */
    void stop();


  protected:

    /* Clear all windows in the main view. */
    void clear();

    /* Checks if the GUI has been stopped.
     *
     * Returns:
     *    true if the GUI has stopped and false otherwise.
     */
    bool has_stopped();

    /* Pauses the gui until a key is pressed. */
    void pause() const;

    /* Refreshes all views. */
    virtual void refresh() const;

    /* Designates the given view to be the currently displayed view and updates
     * it accordingly.
     *
     * Parameters:
     *    view - The view to set as the current view.
     */
    void set_view(View *view);

    /* Updates the current view. */
    void update_view()
    {
      view->update();
    }


  private:

    /* Updates the GUI. Runs every need update for the GUI. Called repeatedly
     * until the GUI is stopped.
     */
    virtual void update() = 0;
};

#endif // GUI_HPP
