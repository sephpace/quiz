/* Author: Seph Pace
 * Email:  sephpace@gmail.com
 */

#include "quiz.hpp"


/* Constructor with deck name */
Quizzer::Quizzer(std::string deck_name)
{
  deck = new Deck(deck_name);
  deck->load(deck_name);
  quiz_view = new QuizView();
  set_view(quiz_view);
}

/* Destructor */
Quizzer::~Quizzer()
{
  delete deck;
  deck = NULL;

  delete quiz_view;
  quiz_view = NULL;
}

/* Determines if the given string is a command and if so, runs the command. */
bool Quizzer::parse_command(const std::string command)
{
  if(command == "q" || command == "quit" || command == "exit")
  {
    stop();
    return true;
  }
  else if(command == "h" || command == "hint")
  {
    // Show the hint
    quiz_view->display_prompt("Hint");
    quiz_view->display_description(card.get_hint());
    update_view();

    // Wait for a key press to continue
    pause();

    // Add the card back to the deck
    deck->add(card);

    return true;
  }
  else if(command == "s" || command == "skip")
  {
    quiz_view->skipped++;
    return true;
  }
  return false;
}

/* Quizzes the user on a single pass of each card in the deck. */
void Quizzer::quiz()
{
  while(deck->size() > 0)
  {
    // Display status info
    quiz_view->remaining = deck->size() + discard.size();
    quiz_view->display_info();

    // Display the top card definition
    card = deck->draw();
    quiz_view->display_prompt("What is\nthis?");
    quiz_view->display_description(card.get_definition());
    update_view();

    // Get answer/command from user
    std::string answer = quiz_view->get_answer();
    answer = tolower(answer);
  
    // Check if the answer is correct
    std::string response;
    if(answer == "")
    {
      // Nothing done, so add the card back to the top of the deck
      deck->add(card);
      continue;
    }
    else if(card.check_answer(answer))
    {
      response = "Correct!";
      quiz_view->correct++;
    }
    else if(parse_command(answer))
    {
      // Quit on stop signal
      if(has_stopped())
      {
        return;
      }

      // No need to display anything else as it's handled in
      // the parse_command method.
      continue; 
    }
    else
    {
      response = "Incorrect!";
      discard.push_back(card);
      quiz_view->incorrect++;
    }

    // Display the response
    response += "\n\nAnswers:";
    for(std::string ans : card.get_answers())
    {
      response += "\n";
      response += ans;
    }
    quiz_view->display_prompt(response);
    update_view();

    // Wait for a key press to continue
    pause();
  }
}

/* Updates the quizzer GUI. */
void Quizzer::update()
{
  // Quizzing pass
  quiz();

  // Check for cards to retry
  if(discard.size() > 0)
  {
    // Shuffle the discard back into the deck
    deck->add_all(discard);
    deck->shuffle();
    discard.clear();
  }
  else
  {
    // Finished
    // TODO: Display finished message
    stop();
  }
}
